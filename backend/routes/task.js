const express = require('express');
const TasksControllers = require('../constrollers/task');

const router = express.Router();

router.post('/create-task', TasksControllers.createTask);
router.post('/get-tasks', TasksControllers.getTasks);
router.post('/tasks', TasksControllers.tasks);
router.post('/my-tasks', TasksControllers.myTasks);
router.post('/update-tasks', TasksControllers.updateTasks);
router.post('/verify-tasks', TasksControllers.verifyTask);

module.exports = router;