const express = require('express');
const AuthControllers = require('../constrollers/auth');

const router = express.Router();

router.post('/signin', AuthControllers.signin);
router.post('/signup', AuthControllers.signup);
router.post('/account', AuthControllers.account);

module.exports = router;
