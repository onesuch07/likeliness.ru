const domain = process.env.NODE_ENV == 'production' ? 'likeliness.ru' : 'localhost';

module.exports = {
  db: 'mongodb://127.0.0.1/likeliness',
  port: 3001,
  secret: 'verysicretkey',
  baseUrl: 'http://localhost'
};
