const app = require('express')();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const session = require('express-session');
const morgan = require('morgan');
const bluebird = require('bluebird');

const config = require('./config');
const authRoute = require('./routes/auth');
const taskRoute = require('./routes/task');
const errorHandler = require('./middleware/errorHandler');
const server = require('http').createServer(app).listen(config.port);
const io = require('socket.io')(server);

require('events').EventEmitter.prototype._maxListeners = 0;

mongoose.Promise = bluebird;
mongoose.set('useUnifiedTopology', true);
mongoose.connect(
  config.db,
  {
    useCreateIndex: true,
    useNewUrlParser: true,
  },
  err => {
    if (err) throw err;
    console.log('MongoDB connected');
  }
);

app.use(morgan('combined'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(
  session({
    resave: true,
    saveUninitialized: true,
    secret: config.secret
  })
);
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, Authorization'
  );
  if (req.method === 'OPTIONS') {
    res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
    return res.status(200).json({});
  }
  next();
});

app.use('/api', authRoute);
app.use('/api', taskRoute);

const chatControllers = require('./constrollers/chats');
let users = 0, 
  addUser = false;

io.on('connection', socket => {
  chatControllers.messages(socket, io);
  chatControllers.chatGetMessage(socket, io);

  socket.on('add user', (username) => {
    if (addUser) return;
    addUser = true;
    io.username = username;
    users++;
  });

  socket.on('disconnect', () => {
    if (addUser) {
      addUser = false;
      users--;

      socket.broadcast.emit('user left', {
        username: socket.username,
        numUsers: numUsers
      });
    }
  });

  socket.on('typing', (id) => {
    socket.broadcast.emit('typing', id);
  });

  socket.on('stop typing', () => {
    socket.broadcast.emit('stop typing');
  });
});

app.use(errorHandler);