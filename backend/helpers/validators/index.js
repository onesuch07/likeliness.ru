const objectTypeStringToLowerCase = async obj => {
  const newObj = obj;
  for (const key in obj) {
    if (typeof obj[key] === 'string') newObj[key] = obj[key].toLowerCase();
  }
  return await newObj;
};

module.exports = {
  objectTypeStringToLowerCase
};