const Chat = require('../models/chats');

const messages = (socket, io) => {
  socket.on('Input Chat Message', async body => {
    const { userId, data } = body;
    const create = {
      ...body,
      listMessages: [ data ]
    };
    
    try {
      const findUser = await Chat.find({ userId });

      if (findUser.length) {
        const createChat = await Chat.updateOne(
          { userId },
          {
            $push: {
              listMessages: {
                $each: [ data ],
                $sort: { dateMessage: -1 }
              }
            }
          }
        );

        if (createChat.ok) {
          Chat.find({ userId }, (err, messages) => {
            if (err) {
              return res.json({
                status: 400,
                message: 'Ошибка обновления модели чата'
              })
            }
            io.emit('Output Chat Message', { messages });
          })
        }
      } else {
        await Chat.create(create, (err, messages) => {
          if (err) {
            return res.json({
              status: 400,
              message: 'Ошибка создания модели чата'
            })
          }
          io.emit('Output Chat Message', { messages });
        })
      }
    } catch (err) {
      console.log('err', err);
    }
  });
};

const chatGetMessage = (socket, io) => {
  socket.on('Chat Get Message', async body => {
    const { userId, userWorkId } = body;
    try {
      if (userWorkId) {
        await Chat.find({ userId }).update({ userWorkId });
        await Chat.find({ userId: userWorkId }, (err, messages) => {
          if (err) {
            return res.json({
              status: 400,
              message: 'Сообщения задачи не найдены',
              errors: err
            })
          }
          io.emit('Output Chat Message', { messages });
        })
      } else {
        await Chat.find({ userId }, (err, messages) => {
          if (err) {
            return res.json({
              status: 400,
              message: 'Сообщения этого аккаунта не найдено',
              errors: err
            })
          }
          io.emit('Output Chat Message', { messages });
        })
      }
    } catch ({ message }) {
      return res.json({
        status: 400,
        message
      })
    }
  });
};

module.exports = {
  messages,
  chatGetMessage
};