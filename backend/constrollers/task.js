const Task = require('../models/task');
const { objectTypeStringToLowerCase } = require('../helpers/validators');

const date = new Date();

const createTask = async (req, res, next) => {
  await objectTypeStringToLowerCase(req.body);
  
  const body = {
    ...req.body,
    status: 1
  };

  try {
    await Task.create(body, (err, result) => {
      if (err) {
        return res.json({
          status: 400,
          message: 'Не смогли создать задачу',
          errors: err.errors
        });
      }
      return res.json(result);
    });
  } catch ({ message }) {
    return next({
      status: 400,
      message
    });
  }
};

const getTasks = async (req, res, next) => {
  try {
    await Task.find((err, result) => {
      if (err) {
        return res.json({
          status: 400,
          message: 'Отфильтрованный список ваших задач пуст',
          errors: err.errors
        });
      }
      return res.json(result);
    });
  } catch (err) {
    return next({
      status: 400,
      message: 'Не удалось получить отфильтрованный список задач'
    });
  }
};

const tasks = async (req, res, next) => {
  const { limit, filter } = req.body;

  await objectTypeStringToLowerCase(filter);
  
  if (filter) {
    try {
      await Task.find({ 
        title: { $regex: filter.title },
        location: { $regex: filter.location }
      }, (err, result) => {
        if (err) {
          return res.json({
            status: 400,
            message: 'Список ваших задач пуст',
            errors: err.errors
          });
        }
        return res.json(result);
      })
        .sort({ taskCreate: -1 })
        .limit(+limit || 20)

    } catch (e) {
      return next({
        status: 400,
        message: 'Не удалось отфильтровать список'
      });
    }
  } else {
    try {
      await Task.find({}, (err, result) => {
        if (err) {
          return res.json({
            status: 400,
            message: 'Список ваших задач пуст',
            errors: err.errors
          });
        }
        return res.json(result);
      })
        .sort({ taskCreate: -1 })
        .limit(+limit || 4)

    } catch (e) {
      return next({
        status: 400,
        message: 'Не удалось получить список ваших задач'
      });
    }
  }
};

// Нужно изменить логику получения сортировки задач
// Если одна из переменных не вернет данные в консоли будет ошибка
const myTasks = async (req, res, next) => {
  const body = {
    tasks: await Task.find({ userId: req.body.id }),
    activeTasks: await Task.find({ status: 1 }),
    completedTasks: await Task.find({ status: 3 })
  };

  try {
    return res.json({
      status: 200,
      body
    })
  } catch (e) {
    return next({
      status: 400,
      message: 'Не удалось получить список моих задач'
    });
  }
};

const updateTasks = async (req, res, next) => {
  const { taskId, status, userTaskProgress } = req.body;
  
  try {
    let taskProgressStarted, taskProgressExpectation, taskProgressClosed;

    if (status === 1) {
      taskProgressStarted = date;
    }
    if (status === 2) {
      taskProgressExpectation = date;
    }
    if (status === 3) {
      taskProgressClosed = date;
    } 
    
    await Task.find({ _id: taskId }, err => {
      if (err) {
        return next({
          status: 400,
          message: 'Активных задач нет'
        });
      }
    }).update({ 
      status,
      userTaskProgress,
      taskProgressStarted,
      taskProgressExpectation,
      taskProgressClosed
    });
    return res.json({ 
      status: 200,
      statusProgress: status
    });
  } catch (e) {
    return next({
      status: 400,
      message: 'Задача уже выполняется'
    });
  }
};

const verifyTask = async (req, res, next) => {
  const { taskId } = req.body;

  try {
    await Task.findOne({ _id: taskId }, err => {
      if (err) {
        return next({
          status: 400,
          message: 'Активных задач нет'
        })
      }
      return res.json({
        status: 200
      });
    }).update({ 
      verify: 1,
      verifyDate: date.setDate(date.getDay() + 14)
    })
  } catch (e) {
    return next({
      status: 400,
      message: 'Не удалось верифицировать'
    })
  }
};

module.exports = {
  createTask,
  tasks,
  myTasks,
  getTasks,
  updateTasks,
  verifyTask
};
