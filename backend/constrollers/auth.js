const User = require('../models/users');

const signin = async (req, res, next) => {
  const { phone, pass } = req.body;
  const credential = {
    phone: phone.replace(/[^0-9]/gim, '')
  };

  const user = await User.findOne({
    phone: credential.phone
  });

  if (!user) {
    return next({
      status: 400,
      message: 'Аккаунт не зарегистрирован'
    });
  }
  
  try {
    if (!await user.comparePasswords(pass)) {
      throw new Error();
    }
    
    req.session.userId = user._id;
    return res.json({
      name: user.name,
      dateReg: user.dateReg,
      _id: user._id,
      phone: user.phone
    });
  } catch (e) {
    return next({
      status: 400,
      message: 'Пароль не верный'
    });
  }
};

const signup = async (req, res, next) => {
  const { phone } = req.body;
  const credential = {
    ...req.body,
    phone: phone.replace(/[^0-9]/gim, ''),
  };
  
  const userPhone = await User.findOne({ phone: credential.phone });
  
  if (userPhone) {
    return next({
      status: 400,
      message: 'Пользователь уже существует'
    });
  }

  try {
    const user = await User.create(credential);
    return res.json({
      name: user.name,
      dateReg: user.dateReg,
      _id: user._id,
      phone: user.phone
    });
  } catch ({ message }) {
    return next({
      status: 400,
      message
    });
  }
};

const account = async (req, res, next) => {
  const { id } = req.body;
  
  try {
    const user = await User.findOne({ _id: id });
    return res.json({
      name: user.name,
      dateReg: user.dateReg,
      _id: user._id,
      phone: user.phone
    });
  } catch ({ message }) {
    return next({
      status: 400,
      message
    });
  }
};

module.exports = {
  signin,
  signup,
  account
};
