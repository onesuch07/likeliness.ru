const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const date = new Date();

const UserSchema = new Schema({
  title: {
    type: String,
    required: true,
    min: 4,
    max: 50
  },
  description: {
    type: String,
    required: true,
    min: 300,
    max: 2000
  },
  file: {},
  category: {
    type: String,
    required: true,
    min: 4,
    max: 40
  },
  userId: {
    index: true,
    type: String,
    required: true
  },
  name: {
    type: String,
    required: true,
    min: 2,
    max: 20
  },
  phone: {
    type: String,
    required: true,
    min: 11,
    max: 11
  },
  price: {
    type: Number,
    required: true,
    min: 100
  },
  professions: {
    type: String,
    required: true,
    min: 2,
    max: 50
  },
  location: {
    type: String,
    required: true,
    min: 2,
    max: 50
  },
  status: {
    type: Number,
    required: true
  },
  // Статус проверки задачи
  verify: {
    type: Number,
    default: 0
  },
  // Интервал проверки выполнения задачи 14 дней (2 недели)
  verifyDate: {
    type: Date,
    default: date.setDate(date.getMonth() + 14)
  },
  // Пользователь выполняющий задачу
  userTaskProgress: {
    type: String,
    min: 20,
    max: 30
  },
  // Временной интервал активности задачи (например 2 недели)
  taskActive: {
    type: Date,
    required: true
  },
  // Время создания задачи
  taskCreate: {
    type: Date,
    default: Date.now
  },
  // Время задачи, приступил к выполнению
  taskProgressStarted: {
    type: Date
  },
  // Время ожидания
  taskProgressExpectation: {
    type: Date
  },
  // Время закрытия задачи
  taskProgressClosed: {
    type: Date
  }
});

module.exports = mongoose.model('Tasks', UserSchema);