const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ChatSchema = new Schema({
  name: {
    type: String,
    required: true,
    min: 2,
    max: 20
  },
  // Пользователь выполняющий задачу
  userWorkId: {
    type: String
  },
  // Пользователь создавший задачу
  userId: {
    index: true,
    type: String,
    required: true
  },
  listMessages: [
    {
      messages: {
        type: String,
        required: true,
        max: 2000
      },
      userId: {
        index: true,
        type: String,
        required: true,
        max: 30
      },
      dateMessage: {
        type: Date,
        default: Date.now
      }
    }
  ],
  messagesFile: {
    type: String
  },
  chatCreate: {
    type: Date,
    default: Date.now
  }
});

module.exports = mongoose.model('Chat', ChatSchema);