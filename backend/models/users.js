const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
  phone: {
    type: String,
    required: true,
    index: true,
    min: 11,
    max: 11
  },
  pass: {
    type: String,
    required: true,
    min: 8,
    max: 50
  },
  name: {
    type: String,
    required: true,
    min: 2,
    max: 20,
    default: 'User_Default_Name',
    lowerCase: true
  },
  dateReg: {
    type: Date,
    default: Date.now()
  },
});

UserSchema.pre('save', async function(next) {
  if (!this.isModified('pass')) return next();

  const salt = await bcrypt.genSalt(10);
  const hash = await bcrypt.hash(this.pass, salt);

  this.pass = hash;
  next();
});

UserSchema.methods.comparePasswords = function(pass) {
  return bcrypt.compare(pass, this.pass);
};

module.exports = mongoose.model('User', UserSchema);
