# Robots

User-agent: *
Disallow: /api/
Disallow: /components/
Disallow: /models/
Disallow: /data/
Disallow: /helpers/
Disallow: /reducers/
Disallow: /selectors/
Disallow: /utils/
Disallow: /assets/

Host: https://likeliness.ru
Sitemap: https://likeliness.ru/sitemap.xml