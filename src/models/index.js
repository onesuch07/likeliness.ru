import header from './header/index';
import modal from './modal/index';
import account from './account/index';
import tasks from './tasks/index';

export default [
  header,
  modal,
  account,
  tasks,
];