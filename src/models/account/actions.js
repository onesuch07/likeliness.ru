export function loadIndex() {
  return {
    type: 'account/loadIndex'
  };
}

export function signIn(user) {
  return {
    type: 'account/signIn',
    user
  };
}

export function signUp(data) {
  return {
    type: 'account/signUp',
    data
  };
}

export function logout() {
  return {
    type: 'account/logout'
  };
}

export function updateState(payload) {
  return {
    type: 'account/updateState',
    payload
  };
}
