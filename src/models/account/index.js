import { put, call, select } from 'redux-saga/effects';
import { reset } from 'redux-form';
import { resetShowBlocks } from 'models/header/actions';
import * as actions from './actions';
import * as api from './api';
import * as selectors from 'selectors';
import { get, set, delByKey } from 'helpers/storage';

const modal = {
  namespace: 'account',
  state: {},
  subscriptions: {
    index(props) {
      props.dispatch(actions.loadIndex());
    }
  },
  effects: {
    *loadIndex() {
      const account = yield select(selectors.account);
      if (Object.keys(account).length < 1 && get('id')) {
        const account = yield call(api.account, { id: get('id') });
        yield put(actions.updateState(account));
      }
    },
    *signIn({ user }) {
      const res = yield call(api.signIn, user);
      yield put(reset('signIn'));
      yield put(resetShowBlocks());
      if (res) {
        yield window.success('Успешный вход');
        yield set('id', res._id);
        yield put(actions.updateState(res));
      }
    },
    *signUp({ data }) {
      const res = yield call(api.signUp, data);
      yield put(reset('signUp'));
      yield put(resetShowBlocks());
      if (res) {
        yield window.success('Успешная регистрация');
        yield set('id', res._id);
        yield put(actions.updateState(res));
      }
    },
    *logout() {
      yield delByKey('id');
      yield put(actions.updateState({
        dateReg: null, name: null, phone: null, _id: null
      }));
      window.success('Успешный выход');
    }
  },
  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload
      }
    }
  }
};

export default modal;
