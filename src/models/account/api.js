import Api from 'api/api';

export function signUp(body) {
  return Api.post('/signup', body);
}

export function signIn(body) {
  return Api.post('/signin', body);
}

export function account(body) {
  return Api.post('/account', body);
}