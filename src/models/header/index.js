import { put, select, delay } from 'redux-saga/effects';
import io from 'socket.io-client';
import * as actions from './actions';
import { openModal } from 'models/modal/actions';
import { getTasks } from 'models/tasks/actions';
import { set, get } from 'helpers/storage';
import * as selectors from 'selectors';
import { reset } from 'redux-form';
import ruRegions from 'data/ru_regions';
import config from 'api/config';

const model = {
  namespace: 'header',
  state: {
    modals: {},
    popups: {
      location: true
    },
    activeCity: 'Регион'
  },
  subscriptions: {},
  effects: {
    *resetShowBlocks() {
      yield put(actions.updateState({
        modals: {}, popups: {}
      }));
    },
    *modalSignIn({ signIn }) {
      yield put(actions.resetShowBlocks());
      yield put(openModal());
      yield put(actions.updateState({ modals: { signIn } }));
    },
    *modalSignUp({ signUp }) {
      yield put(actions.resetShowBlocks());
      yield put(openModal());
      yield put(actions.updateState({ modals: { signUp } }));
    },
    *modalCreateTask({ createTask }) {
      yield put(actions.resetShowBlocks());
      yield put(openModal());
      yield put(actions.updateState({ modals: { createTask } }));
    },
    *modalYouPerformer({ youPerformer }) {
      yield put(actions.resetShowBlocks());
      yield put(openModal());
      yield put(actions.updateState({ modals: { youPerformer } }));
    },
    *modalSearchCity({ searchCity }) {
      yield put(actions.resetShowBlocks());
      yield put(openModal());
      yield put(actions.updateState({ modals: { searchCity } }));
    },
    *popupAccountMenu() {
      const state = yield select(selectors.header);
      yield put(actions.resetShowBlocks());
      yield put(actions.updateState({ 
        popups: { accountMenu: !state.popups.accountMenu }
      }));
    },
    *popupLocation() {
      const state = yield select(selectors.header);
      yield put(actions.resetShowBlocks());
      yield put(actions.updateState({ 
        popups: { location: !state.popups.location }
      }));
    },
    *modalChat({ chat }) {
      yield put(actions.resetShowBlocks());
      yield put(openModal());
      yield put(actions.getMyMessages());
      yield put(actions.updateState({ modals: { chat } }));
    },
    *filterRegions({ value }) {
      if (!value) return;
      const regions = yield ruRegions.filter(el => el.city.toLowerCase() === value.toLowerCase());
      yield put(actions.updateState({ regions }));
      if (regions && regions[0] && regions[0].city) {
        const activeCity = regions[0].city;
        set('city', activeCity);
        yield put(getTasks(activeCity));
        yield put(actions.updateState({ activeCity: get('city') }));
      }
    },
    *typing() {
      const state = yield select(selectors.account);
      io.id = state._id;
      io(config.baseUrl).emit('typing', {
        userId: io.id
      });
      yield put(actions.updateState({ userTyping: true }));
      yield delay(1000);
      io(config.baseUrl).emit('stop typing');
      yield put(actions.updateState({ userTyping: false }));
    },
    *getMyMessages() {
      const account = yield select(selectors.account);
      const header = yield select(selectors.header);
      if (!Object.keys(account).length) {
        window.warning('Необходимо войти');
        yield put(actions.modalSignIn());
      } else {
        io(config.baseUrl).emit('Chat Get Message', {
          userId: account['_id'],
          userWorkId: header.userWorkId
        })
      }
    },
    *messageEvent({ data }) {
      const account = yield select(selectors.account);
      if (!Object.keys(account).length) {
        window.warning('Необходимо войти');
        yield put(actions.modalSignIn());
      } else {
        yield put(reset('Chat'));
        data.userId = account['_id'];
        io(config.baseUrl).emit('Input Chat Message', {
          data,
          name: io.username,
          userId: account['_id']
        })
      }
    }
  },
  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload
      };
    }
  }
};

export default model;
