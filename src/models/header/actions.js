export function resetShowBlocks() {
  return {
    type: 'header/resetShowBlocks'
  };
}

export function popupAccountMenu() {
  return {
    type: 'header/popupAccountMenu'
  }
}

export function popupLocation() {
  return {
    type: 'header/popupLocation'
  }
}

export function typing() {
  return {
    type: 'header/typing'
  }
}

export function modalSignIn() {
  return {
    type: 'header/modalSignIn',
    signIn: true
  };
}

export function modalSignUp() {
  return {
    type: 'header/modalSignUp',
    signUp: true
  };
}

export function getMyMessages() {
  return {
    type: 'header/getMyMessages'
  };
}

export function messageEvent(data) {
  return {
    type: 'header/messageEvent',
    data
  };
}

export function modalSearchCity() {
  return {
    type: 'header/modalSearchCity',
    searchCity: true
  };
}

export function modalCreateTask() {
  return {
    type: 'header/modalCreateTask',
    createTask: true
  };
}

export function modalYouPerformer() {
  return {
    type: 'header/modalYouPerformer',
    youPerformer: true
  };
}

export function modalChat() {
  return {
    type: 'header/modalChat',
    chat: true
  };
}

export function filterRegions(value) {
  return {
    type: 'header/filterRegions',
    value
  };
}

export function updateState(payload) {
  return {
    type: 'header/updateState',
    payload
  };
}
