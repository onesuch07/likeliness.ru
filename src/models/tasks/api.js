import Api from 'api/api';

export function createTask(body) {
  return Api.post('/create-task', body);
}

export function tasks(body) {
  return Api.post('/tasks', body);
}

export function getTasks() {
  return Api.post('/get-tasks');
}

export function myTasks(body) {
  return Api.post('/my-tasks', body);
}

export function updateTask(body) {
  return Api.post('/update-tasks', body);
}

export function verifyTasks(body) {
  return Api.post('/verify-tasks', body);
}