export function viewTasks() {
  return {
    type: 'tasks/viewTasks'
  };
}

export function filterToTasks(filter) {
  return {
    type: 'tasks/filterToTasks',
    filter
  };
}

export function getTasks(city) {
  return {
    type: 'tasks/getTasks',
    city
  };
}

export function userTaskUpdate(taskId, status, userId, userTaskProgress, verify) {
  return {
    type: 'tasks/userTaskUpdate',
    data: {
      taskId,
      status,
      userId,
      userTaskProgress,
      verify
    }
  };
}

export function updateTask(data) {
  return {
    type: 'tasks/updateTask',
    data
  };
}

export function createTask(task) {
  return {
    type: 'tasks/createTask',
    task
  };
}

export function myTasks() {
  return {
    type: 'tasks/myTasks'
  };
}

export function updateState(payload) {
  return {
    type: 'tasks/updateState',
    payload
  };
}
