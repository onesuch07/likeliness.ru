import { put, call, select } from 'redux-saga/effects';
import { reset } from 'redux-form';
import { resetShowBlocks } from 'models/header/actions';
import listenHistory from 'utils/history';
import * as routers from 'helpers/routers';
import history from 'local-history';
import * as actions from './actions';
import { modalSignIn, modalYouPerformer, getMyMessages } from 'models/header/actions';
import * as api from './api';
import * as selectors from 'selectors';
import { set } from 'helpers/storage';

const modal = {
  namespace: 'tasks',
  state: {},
  subscriptions: {
    load(props) {
      const cb = () => props.dispatch(actions.getTasks());
      listenHistory(props, routers.defaultRouting(), cb);
    },
    tasks(props) {
      const cb = () => props.dispatch(actions.viewTasks());
      listenHistory(props, routers.tasks(), cb);
    },
    loadMyTasks(props) {
      const cb = () => props.dispatch(actions.myTasks());
      listenHistory(props, routers.myTasksActive(), cb);
      listenHistory(props, routers.myTasksCompleted(), cb);
    },
    loadPay(props) {
      const cb = () => props.dispatch(actions.updateTask());
      listenHistory(props, routers.pay(), cb);
    },
  },
  effects: {
    *getTasks({ city }) {
      const tasks = yield call(api.tasks, { city });
      yield put(actions.updateState({ tasks }));
    },
    *filterToTasks({ filter }) {
      const tasks = yield call(api.tasks, { filter });
      yield put(actions.updateState({ tasks }));
      window.success('Задачи отфильтрованы');
    },
    *viewTasks() {
      const tasks = yield call(api.getTasks);
      yield put(actions.updateState({ tasks }));
    },
    *myTasks() {
      const account = yield select(selectors.account);
      const { body } = yield call(api.myTasks, { id: account['_id']});
      yield put(actions.updateState(body));
    },
    *createTask({ task }) {
      const account = yield select(selectors.account);
      yield call(api.createTask, {
        ...task,
        price: +task.price,
        phone: account.phone,
        name: account.name,
        userId: account['_id']
      });
      yield put(reset('createTask'));
      yield put(resetShowBlocks());
      yield put(actions.updateState({ task }));
      yield put(actions.getTasks());
    },
    *userTaskUpdate({ data }) {
      const account = yield select(selectors.account);
      if (!Object.keys(account).length) {
        window.warning('Необходимо войти');
        yield put(modalSignIn());
      } else if (account['_id'] === data.userId && !data.verify) {
        window.warning('Решать свои задачи невозможно');
      } else if (data.verify) {
        yield call(api.verifyTasks, {
          taskId: data.taskId
        });
        yield put(actions.getTasks());
        window.warning('Задача проверяется');
      } else if (data.userTaskProgress && account['_id'] !== data.userTaskProgress) {
        window.warning('Задачу решают');
      } else {
        if (data.status === 2) {
          yield put(actions.updateTask(data));
        }
      }
    },
    *updateTask({ data }) {
      const account = yield select(selectors.account);
      const tasks = yield select(selectors.tasks);
      if (data && Object.keys(tasks).length && Object.keys(account).length) {
        yield history.push(routers.pay());
        const statusTasks = yield call(api.updateTask, {
          taskId: data.taskId,
          status: data.status,
          userTaskProgress: account['_id']
        });
        set('userWorkId', data.userId);
        yield put(actions.updateState({ userWorkId: data.userId, tasks }));
        yield put(getMyMessages());
        yield put(actions.getTasks());
        if (statusTasks && statusTasks['statusProgress'] === 2) {
          yield put(modalYouPerformer());
        }
      }
    }
  },
  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload
      };
    },
  },
};

export default modal;
