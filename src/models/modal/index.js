import { put } from 'redux-saga/effects';
import * as actions from './actions';

const modal = {
  namespace: 'modal',
  state: {
    modal: false,
  },
  effects: {
    *openModal({ modal }) {
      yield put(actions.updateState({ modal }));
    },
    *closeModal({ modal }) {
      yield put(actions.updateState({ modal }));
    },
  },
  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    },
  },
};

export default modal;
