export function openModal() {
  return {
    type: 'modal/openModal',
    modal: true,
  };
}

export function closeModal() {
  return {
    type: 'modal/closeModal',
    modal: false,
  };
}

export function updateState(payload) {
  return {
    type: 'modal/updateState',
    payload,
  };
}
