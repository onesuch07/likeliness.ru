import React from 'react';
import { renderRoutes } from 'react-router-config';
import * as routers from 'helpers/routers/index';
import App from 'pages/app';

function Root({ location, route }) {
  return <App location={location}>{renderRoutes(route.routes)}</App>;
}

const list = [
  {
    path: routers.defaultRouting(),
    exact: true,
    component: require('pages/mainPage/Container').default,
  }, 
  {
    path: routers.tasks(),
    exact: true,
    component: require('pages/tasks/Container').default
  },
  {
    path: routers.myTasks(),
    component: require('pages/myTasks/Container').default,
    routes: [
      {
        path: routers.myTasksActive(),
        component: require('pages/myTasks/components/active/Container').default
      },
      {
        path: routers.myTasksCompleted(),
        component: require('pages/myTasks/components/completed/Container').default
      },
      {
        path: routers.defaultRouting(),
        component: require('pages/myTasks/Container').default
      }
    ]
  },
  {
    path: routers.pay(),
    exact: true,
    component: require('components/pay/Container').default
  },
  {
    component: require('pages/notFound').default
  },
];

const rootRoute = [
  {
    component: Root,
    routes: list,
  },
];

export default renderRoutes(rootRoute);
