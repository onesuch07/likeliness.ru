import { combineReducers } from 'redux';
import models from 'models';
import prefixNameSpace from 'utils/prefixNameSpace';
import { reducer as formReducer } from 'redux-form';
import getReducer from 'utils/getReducer';

const reducers = {
  form: formReducer,
};

for (let index = 0; index < models.length; index++) {
  const model = prefixNameSpace(models[index]);
  if (model.reducers) {
    reducers[model.namespace] = getReducer(model.reducers, {});
  }
}

const rootReducer = combineReducers(reducers);

export default rootReducer;
