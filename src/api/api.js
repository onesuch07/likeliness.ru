import config from './config';
import headers from './headers';

const response = res => {
  if (res.ok) {
    return res.json();
  } else if (res.status >= 500) {
    window.error('Ошибка сервера');
    throw new Error();
  } else if (res.status >= 400 && res.status < 500) {
    res.json().then(json => window.error(json.message));
  }
};

const location = () => fetch('http://api.sypexgeo.net/json'
).then(res => response(res));

const get = path => {
  const url = `${config.baseUrl}${config.router}${path}`;
  return fetch(url, {
    method: 'GET',
    headers: headers()
  }).then(res => response(res));
};

const post = (path, body) => {
  const url = `${config.baseUrl}${config.router}${path}`;
  return fetch(url, {
    method: 'POST',
    headers: headers(),
    body: JSON.stringify(body),
  }).then(res => response(res));
};

export default {
  get,
  post,
  location
};
