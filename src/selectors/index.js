export const app = (state) => state.app;
export const wrapper = (state) => state.wrapper;
export const account = (state) => state.account;
export const header = (state) => state.header;
export const tasks = (state) => state.tasks;
