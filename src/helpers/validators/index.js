import moment from 'moment';
import { userText } from 'data/user-write';
const date = new Date();

export const dateTo = min => value =>
  value && Date.parse(value) <= Date.parse(min)
    ? 'The date has not yet arrived'
    : undefined;
export const dateTo1 = dateTo(moment(date).add(1, 'd').toDate());
export const dateInThe = (min, max) => value =>
  value && value < min && value < max
    ? 'The date has not yet arrived'
    : undefined;
export const date5to30 = dateInThe(moment(date).add(5, 'd').toDate(), 30);
export const required = value => value ? undefined : 'Required';
export const maxLength = max => value =>
    value.length > max ? `Must be ${max} characters or less` : undefined;
export const maxLength12 = maxLength(12);
export const maxLength13 = maxLength(13);
export const maxLength15 = maxLength(15);
export const maxLength16 = maxLength(16);
export const maxLength20 = maxLength(20);
export const maxLength30 = maxLength(30);
export const number = value => isNaN(Number(value)) ? 'Must be a number' : undefined;
export const minLength = min => value => 
  value.length < min ? `Must be at least ${min}` : undefined;
export const minLength2 = minLength(2);
export const minLength3 = minLength(3);
export const minLength8 = minLength(8);
export const minLength11 = minLength(11);
export const minLength16 = minLength(16);
export const minLength18 = minLength(18);
export const minLength20 = minLength(20);
export const more = max => value => 
  value < max ? `Value more ${max}` : undefined;
export const smaller = min => value =>
  value > min ? `Value smaller ${min}` : undefined;
export const more100 = more(100);
export const smaller00 = smaller(100);
export const email = (value) =>
    !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? 'Invalid email address'
    : undefined;
export const password = value =>
    !/(?=.*[0-9])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{6,}/g.test(value)
    ? 'Invalid password'
    : undefined;
export const charactersLocked = value => /[<>/]/.test(value) ? 'Characters locked' : undefined;
export const tooOld = value => value > 65 ? 'You might be too old for this' : undefined;
export const aol = value => 
    /.+@aol\.com/.test(value) 
    ? 'Really? You still use AOL for your email?'
    : undefined;
export const nameRu = value => !/^[А-Яа-яЁё]+$/.test(value) ? 'Name no valid' : undefined;
export const uncensored = value => userText(value) ? 'Uncensored' : undefined;