import { isObject } from 'lodash';
import cookies from 'js-cookie';

export function set(key, value) {
  if (!key && !value) return;
  if (isObject(value)) JSON.stringify(value);
  cookies.set(key, value, { path: '', expires: 365 });
}

export function get(key) {
  if (!key) return;
  try {
    return JSON.parse(cookies.get(key));
  } catch (err) {
    return cookies.get(key);
  }
}

export function delByKey(key, path = '') {
  try {
    cookies.remove(key, { path: path });
  } catch (err) {}
}

export function delAll(key, path = '', domain) {
  try {
    cookies.remove(key, { path: path, domain: `.${domain}` });
  } catch (err) {}
}