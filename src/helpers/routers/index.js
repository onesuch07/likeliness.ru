export const defaultRouting = () => '/';
export const pay = () => '/pay';
export const tasks = () => '/tasks';
export const myTasks = () => '/my-tasks';
export const myTasksActive = () => `${myTasks()}/active`;
export const myTasksCompleted = () => `${myTasks()}/completed`;
