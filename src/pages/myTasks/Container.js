import { bindActionCreators, compose } from 'redux';
import { connect } from 'react-redux';
import * as actions from 'models/header/actions';
import * as routers from 'helpers/routers';
import { set, get } from 'helpers/storage';
import index from './index';

const tabs = [
  {
    id: 0,
    title: 'Актуальные',
    path: routers.myTasksActive()
  },
  {
    id: 1,
    title: 'Завершенные',
    path: routers.myTasksCompleted()
  }
];

const mapStateToProps = (state) => ({
  ...state.tasks,
  tabs,
  activeTab: get('activeTab') || tabs[0]
});

const mapDispatchToProps = (dispatch) => ({
  tabEvent: (tab, i) => {
    tab.id = i;
    set('activeTab', tab);
  },
  ...bindActionCreators(actions, dispatch)
});

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(index);
