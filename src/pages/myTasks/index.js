import React from 'react';
import { renderRoutes } from 'react-router-config';
import { Redirect } from 'react-router-dom';
import * as routers from 'helpers/routers';
import './styles.less';

import Tabs from 'components/tabs';

function index({ tabs, route, tabEvent, activeTab }) {
  return (
    <div className="my-tasks">
      <Tabs
        tabs={tabs}
        tabEvent={tabEvent}
        activeTab={activeTab}
      />
      <Redirect to={activeTab ? activeTab.path : routers.myTasksActive()} />
      {renderRoutes(route.routes)}
    </div>
  );
}

export default index;
