import React from 'react';

import Task from 'components/task';
import Clean from 'components/clean';

function index({ activeTasks, userId, userTaskUpdate }) {
  return activeTasks && activeTasks.length > 1 ? (
    <Task tasks={activeTasks} userTaskUpdate={userTaskUpdate} userId={userId} />
  ) : <Clean message="Список активных задач пуст" />;
}

export default index;
