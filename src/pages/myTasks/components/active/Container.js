import { compose, bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from 'models/tasks/actions';
import index from './index';

const mapStateToProps = state => ({
  activeTasks: state.tasks.activeTasks,
  userId: state.account['_id']
});

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators(actions, dispatch)
});

export default compose(connect(
  mapStateToProps,
  mapDispatchToProps
))(index);
