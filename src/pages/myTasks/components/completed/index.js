import React from 'react';

import Task from 'components/task';
import Clean from 'components/clean';

function index({ completedTasks, userId, userTaskUpdate }) {
  return completedTasks && completedTasks.length > 0 ? (
    <Task tasks={completedTasks} userTaskUpdate={userTaskUpdate} userId={userId} />
  ) : <Clean message="Список завершенных задач пуст" />;
}

export default index;
