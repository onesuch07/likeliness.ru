import React from 'react';
import Header from 'components/header/Container';
import Footer from 'components/footer';

function index({ children }) {
  return (
    <div className="app">
      <div className="max-size-windows">
        <Header />
        {children}
        <Footer />
      </div>
    </div>
  );
}

export default index;
