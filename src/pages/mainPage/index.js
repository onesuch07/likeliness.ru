import React from 'react';
import LocationSearch from 'pages/mainPage/locationSearch/Container';
import Advantages from 'components/advantages';
import LastAssignments from 'pages/mainPage/lastAssignments';

function index({ tasks, userTaskUpdate, userId }) {
  return (
    <React.Fragment>
      <LocationSearch />
      <Advantages />
      <LastAssignments
        tasks={tasks}
        userTaskUpdate={userTaskUpdate}
        userId={userId}
      />
    </React.Fragment>
  );
}

export default index;
