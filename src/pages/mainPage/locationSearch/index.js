import React from 'react';
import { Field } from 'redux-form';
import { maxLength30, required } from 'helpers/validators';
import './style.less';

import Form from 'components/form';
import FC from 'components/form/components';
import Button from 'components/button';
import UserWork from 'assets/svg-images/user-work';

function LocationSearch({ handleSubmit, city, location }) {
  return (
    <div className="app-location-search">
      <div className="location-search-work-user">
        <div className="location-search-description">
          <p>Работа на 2-3 часа</p>
        </div>
        <div className="location-search-title">
          <h2>Найти работу по душе</h2>
        </div>
        <Form
          handleSubmit={handleSubmit}
          className="location-search"
          row
          icon
        >
          <Field
            component={FC.InputDataList}
            validate={[required, maxLength30]}
            name="title"
            icon="fa-search"
            type="text"
            placeholder="Название вакансии"
            value={city}
          />
          <Field
            component={FC.InputDataList}
            validate={[required, maxLength30]}
            name="location"
            icon="fa fa-map-marker"
            type="text"
            placeholder="Местоположение"
            value={location}
          />
          <Button type="submit" label="Найти работу" />
        </Form>
      </div>
      <UserWork />
    </div>
  );
}

export default LocationSearch;
