import { compose, bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import * as actions from 'models/tasks/actions';
import index from './index';

const mapDispatchToProps = (dispatch) => ({
  ...bindActionCreators(actions, dispatch)
});

export default compose(
  connect(mapDispatchToProps),
  reduxForm({
    enableReinitialize: false,
    form: 'citySearchByTasks',
    onSubmit: (value, dispatch) => dispatch(actions.filterToTasks(value))
  })
)(index);
