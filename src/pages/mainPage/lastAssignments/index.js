import React from 'react';
import { Link } from 'react-router-dom';
import * as routers from 'helpers/routers';
import './style.less';

import Task from 'components/task';
import Clean from 'components/clean';

function index({ tasks, userTaskUpdate, userId }) {
  return (
    <div className="app-last-assignments">
      <h2>Последние задания</h2>
      {tasks && tasks.length > 0 ? (
        <Task tasks={tasks} userTaskUpdate={userTaskUpdate} userId={userId} />
      ) : <Clean message="В вашем регионе нет активных задач" />}
      <Link to={routers.tasks()} className="all-announcements">
        Все объявления
      </Link>
    </div>
  );
}

export default index;
