import React from 'react';
import './styles.less';

function index() {
  return (
    <div className="not-found">
      <span>404</span>
      <p>Страница не найдена</p>
    </div>
  );
}

export default index;
