import React from 'react';
import Task from 'components/task';
import { Field } from 'redux-form';
import { required, maxLength20 } from 'helpers/validators';
import './styles.less';

import Form from 'components/form';
import FC from 'components/form/components';

function index({ tasks, handleSubmit, invalid, anyTouched, userTaskUpdate }) {
  return (
    <div className="all-tasks">
      <div className="search-tasks">
        <Form handleSubmit={handleSubmit} row icon>
          <Field
            component={FC.Input}
            validate={[required, maxLength20]}
            icon="fa-search"
            name="title"
            type="text"
            placeholder="Поиск заказов"
          />
          <Field
            component={FC.Input}
            validate={[required, maxLength20]}
            icon="fa-map-marker"
            name="location"
            type="text"
            placeholder="Местоположение"
          />
          <Form.F
            anyTouched={anyTouched}
            invalid={invalid}
            btnName="Найти работу"
            type="submit"
            btn240 
            btnT
          />
        </Form>
      </div>
      <div className="show-tasks">
        {
          !tasks ? 'Не удалось получить задачи' : tasks.length < 1 ?
          <div>Список задач пустой</div> : (
            <Task
              mwnone
              tasks={tasks}
              userTaskUpdate={userTaskUpdate}
            />
          )
        }
      </div>
    </div>
  );
}

export default index;
