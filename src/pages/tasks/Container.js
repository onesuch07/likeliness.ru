import { compose, bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import * as actions from 'models/tasks/actions';
import index from './index';

const mapStateToProps = state => ({
  ...state.tasks
});

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators(actions, dispatch)
});

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  reduxForm({
    enableReinitialize: false,
    form: 'searchTasks',
    onSubmit: (value, dispatch) => dispatch(actions.filterToTasks(value))
  })
)(index);
