import React from 'react';
import './styles.less';

function index({ message }) {
  return (
    <div className="clean">{message}</div>
  )
}

export default index;