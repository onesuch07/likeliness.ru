import { compose, bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import * as actionsAccount from 'models/account/actions';
import * as actionsHeaders from 'models/header/actions';
import index from './index';

const mapStateToProps = (state) => ({
  ...state.account,
});

const mapDispatchToProps = (dispatch) => ({
  ...bindActionCreators({
    ...actionsAccount,
    ...actionsHeaders
  }, dispatch)
});

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  reduxForm({
    enableReinitialize: false,
    form: 'signUp',
    onSubmit: (value, dispatch) => dispatch(actionsAccount.signUp(value)),
  })
)(index);
