import React from 'react';
import { Field } from 'redux-form';
import {
  required,
  minLength2,
  minLength8,
  minLength16,
  maxLength16,
  password,
  nameRu,
  maxLength15,
  maxLength30,
  uncensored,
  charactersLocked
} from 'helpers/validators';
import { phone } from 'helpers/masks';
import Form from 'components/form';
import FC from 'components/form/components';
import Modal from 'components/modal/Container';

function index({ handleSubmit, modalSignIn, invalid, anyTouched }) {
  return (
    <Modal>
      <Form handleSubmit={handleSubmit}>
        <Form.H
          titleH="Регистрация"
          titleD="Мы вышлем на Ваш мобильный телефон бесплатное сообщение с кодом"
        />

        <Field
          component={FC.Input}
          validate={[required, minLength2, nameRu, maxLength15, uncensored, charactersLocked]}
          name="name"
          type="text"
          placeholder="Имя"
          label="Ваше имя"
        />

        <Field
          component={FC.Input}
          validate={[required, password, minLength8, maxLength30, uncensored, charactersLocked]}
          name="pass"
          type="password"
          placeholder="Введите пароль"
          label="Пароль"
        />

        <Field
          component={FC.Input}
          validate={[required, minLength16, maxLength16, uncensored, charactersLocked]}
          name="phone"
          type="text"
          placeholder="+7(900)000-00-00"
          label="Номер телефона"
          onChange={e => phone.mask(e.target)}
        />

        <Form.F
          anyTouched={anyTouched}
          invalid={invalid}
          descriptionF="Если у вас уже есть учетная запись"
          urlName="Войдите"
          btnName="Зарегистрироваться"
          type="submit"
          event={modalSignIn}
          btn240
        />
      </Form>
    </Modal>
  );
}

export default index;
