import React from 'react';
import classnames from 'classnames';
import './styles.less';

function Modal({ modal, closeModal, children, mw, paddingNone }) {
  const maxWidth = mw || 'mw575';
  const paddingDisable = paddingNone && 'disable-padding';
  return modal ? (
    <div className="modal">
      <div className={classnames('modal-panel', maxWidth, paddingDisable)}>
        <div className="close-modal">
          <div className="event-close">
            <svg onClick={() => closeModal()}><use xlinkHref="/sprite.svg#close"/></svg>
          </div>
        </div>
        {children}
      </div>
    </div>
  ) : null;
}

export default Modal;
