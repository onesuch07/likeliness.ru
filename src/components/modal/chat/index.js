import React from 'react';
import { Field } from 'redux-form';
import io from 'socket.io-client';
import config from 'api/config';
import { map } from 'lodash/collection';
import { minLength3, required, charactersLocked } from 'helpers/validators';
import classnames from 'classnames';
import './styles.less';

import Modal from 'components/modal/Container';
import Form from 'components/form';
import Button from 'components/button';
import FC from 'components/form/components';
import LiveUserWrite from 'components/liveUserWrite';

class Chat extends React.PureComponent {
  state = {
    usersChat: null,
    messages: null,
    userId: null,
    userTyping: false
  };

  _isDidUpdate = false;
  
  componentDidUpdate() {
    this._isDidUpdate = true;
    const { id } = this.props;
    this.socket = io(config.baseUrl);
    this.socket.on('Output Chat Message', res => {
      if(this._isDidUpdate) {
        const usersChat = res.messages;
        map(usersChat, (el) => {
          if (id === el.userId) {
            const messages = el.listMessages;
            this.setState({ messages });
          }
        });
        this.setState({ usersChat });
      }
    });
    this.socket.on('typing', ({ userId }) => {
      this.setState({ userTyping: true, userId })
    });
    this.socket.on('stop typing', () => {
      this.setState({ userTyping: false })
    });
  }

  activeMenu(i) {
    const chatActive = document.querySelector('.chat-active');
    const chatUserList = document.querySelectorAll('.chat-user-list');
    if (chatActive && chatUserList) {
      chatActive.classList.remove('chat-active');
      chatUserList[i].classList.add('chat-active');
    }
  }

  render() {
    const { handleSubmit, id } = this.props;
    const { usersChat, messages, userId, userTyping } = this.state;
    return (
      <Modal mw="mw1053" paddingNone>
        <div className="app-chat">
          <div className="chat-users">
            {usersChat && (usersChat.length < 1 || !Array.isArray(usersChat)) ? (
                <div className={classnames('chat-user-list', 'chat-active')}>
                  <div className="chat-username">
                    <p>Демо пользователь</p>
                  </div>
                  <div className="chat-activity">Был в сети 00:00</div>
                </div>
            ) :
              map(usersChat, (el, i) => (
                <div
                  className={classnames(
                    'chat-user-list',
                    i === 0 && 'chat-active'
                  )}
                  key={i}
                  onClick={() => this.activeMenu(i)}
                >
                  <div className="chat-username">
                    <p>{el.name}</p>
                    <span>{el.listMessages.length}</span>
                  </div>
                  <div className="chat-activity">Был в сети 3 часа назад</div>
                </div>
              ))
            }
          </div>
          <div className="chat-messages">
            <div className="chat-user-container-messages">
              <div className="chat-messages-title-date">
                <span>сегодня</span>
              </div>
              <div className="chat-massages-body">
                {messages && (messages < 1 || !Array.isArray(messages)) ? null :
                  map(messages, (el, i) => (
                    <div
                      key={i}
                      className={classnames(
                        'chat-user-messages',
                        id === el.userId && 'my'
                      )}>
                      <div className="chat-container-messages">
                        <p>{el.messages}</p>
                        <div className="chat-clock">{el.dateMessage}</div>
                      </div>
                    </div>
                  ))
                }
              </div>
            </div>
            {userTyping && userId !== id && <LiveUserWrite />}
            <Form handleSubmit={handleSubmit}>
              <Field
                component={FC.TextAreaInputFile}
                validate={[required, minLength3, charactersLocked]}
                name="messages"
                placeholder="Введите сообщение"
              />
              <div className="container-button">
                <Button
                  type="submit"
                  label="Отправить"
                  className="white"
                />
              </div>
            </Form>
          </div>
        </div>
      </Modal>
    )
  }
}

export default Chat;