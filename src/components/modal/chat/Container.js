import { compose, bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import * as actions from 'models/header/actions';
import Index from './index';

const mapStateToProps = state => ({
  ...state.tasks,
  id: state.account._id,
  messages: state.header.messages,
  userTyping: state.header.userTyping
});

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators(actions, dispatch)
});

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  reduxForm({
    enableReinitialize: false,
    form: 'Chat',
    onChange: (value, dispatch) => dispatch(actions.typing()),
    onSubmit: (value, dispatch) => dispatch(actions.messageEvent(value))
  })
)(Index);
