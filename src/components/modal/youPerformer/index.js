import React from 'react';
import Form from 'components/form';
import Modal from 'components/modal/Container';

function index({ modalChat }) {
  return (
    <Modal>
      <Form.H
        titleH="Вы стали исполнителем!"
        titleD="Для уточнения деталий задания, необходимо связаться с заказчиком"
      />
      <Form.F
        btnName="Чат с заказчиком"
        event={modalChat}
        runFunc
        btn240
      />
    </Modal>
  );
}

export default index;