import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from 'models/header/actions';
import index from './index';

const mapStateToProps = (state) => ({
  ...state.header
});

const mapDispatchToProps = (dispatch) => ({
  ...bindActionCreators(actions, dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(index);