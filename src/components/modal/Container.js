import { compose, bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from 'models/modal/actions';
import Modal from './index';

const mapStateToProps = (state) => ({
  ...state.modal
});

const mapDispatchToProps = (dispatch) => ({
  ...bindActionCreators(actions, dispatch),
});

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(Modal);
