import React from 'react';
import { Field } from 'redux-form';
import {
  required,
  minLength8,
  minLength16,
  maxLength16,
  password,
  uncensored,
  charactersLocked
} from 'helpers/validators';
import { phone } from 'helpers/masks';
import Modal from 'components/modal/Container';
import Form from 'components/form';
import FC from 'components/form/components';

function index({ handleSubmit, modalSignUp, invalid, anyTouched }) {
  return (
    <Modal>
      <Form handleSubmit={handleSubmit}>
        <Form.H titleH="Вход"/>
        
        <Field
          component={FC.Input}
          validate={[required, minLength16, maxLength16, uncensored, charactersLocked]}
          name="phone"
          type="text"
          placeholder="+7(900)000-00-00"
          label="Номер телефона"
          onChange={e => phone.mask(e.target)}
        />

        <Field
          component={FC.Input}
          validate={[required, password, minLength8, uncensored, charactersLocked]}
          name="pass"
          type="password"
          placeholder="Введите пароль"
          label="Пароль"
        />

        <Form.F
          anyTouched={anyTouched}
          invalid={invalid}
          descriptionF="Еще нет аккаунта?"
          urlName="Зарегистрируйтесь"
          btnName="Войти"
          type="submit"
          event={modalSignUp}
          btn240
        />
      </Form>
    </Modal>
  );
}

export default index;
