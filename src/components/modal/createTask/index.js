import React, { useState } from 'react';
import { Field } from 'redux-form';
import {
  required,
  minLength8,
  minLength3,
  minLength2,
  number,
  more100,
  dateTo1,
  uncensored,
  charactersLocked
} from 'helpers/validators';
import Modal from 'components/modal/Container';
import Form from 'components/form';
import FC from 'components/form/components';
import * as i18n from './i18n';

function Index(props) {
  const [showMoreCategories, setShowMoreCategories] = useState(false);
  const [moreCategories, setMoreCategories] = useState(null);

  const showCategoriesMore = (event) => {
    const find = i18n.categories.filter((e) =>
      event.target.value.toLowerCase().includes(e.name.toLowerCase())
    );

    setShowMoreCategories(true);
    setMoreCategories(find ? find[0].more : moreCategories);
  };

  const { handleSubmit, filterRegions, findCity, invalid, anyTouched } = props;
  return (
    <Modal mw="mw900">
      <Form handleSubmit={handleSubmit}>
        <Form.H titleH="Новое задание" />

        <Field
          component={FC.Input}
          validate={[required, minLength3, uncensored, charactersLocked]}
          name="title"
          type="text"
          label="Название вакансии"
        />

        <Field
          component={FC.TextArea}
          validate={[required, minLength8, uncensored, charactersLocked]}
          name="description"
          type="text"
          label="Описание (минимум 300 символов)"
        />

        <Field
          component={FC.File}
          name="file"
          type="file"
          label="Добавить файл"
        />

        <div className="label-row">
          <Field
            component={FC.Select}
            validate={[required]}
            name="category"
            label="Категория"
            items={i18n.categories}
            onChange={showCategoriesMore}
            onFocus={showCategoriesMore}
          />

          <Field
            component={FC.Input}
            validate={[required, number, more100]}
            name="price"
            type="text"
            label="Бюджет (руб.)"
          />
        </div>

        {showMoreCategories ? (
          <Field
            component={FC.Select}
            validate={[required, minLength3]}
            name="professions"
            label="Специализация"
            items={moreCategories}
          />
        ) : null}

        <div className="label-row">
          <Field
            component={FC.InputDataList}
            validate={[required, minLength2, uncensored, charactersLocked]}
            name="location"
            type="text"
            label="Местоположение (при необходимости)"
            onChange={() => filterRegions}
            options={findCity}
            objKey="city"
          />

          <Field
            component={FC.InputDate}
            validate={[required, dateTo1]}
            name="taskActive"
            label="Актуален до"
          />
        </div>

        <Form.F
          anyTouched={anyTouched}
          invalid={invalid}
          btnName="Разместить вакансию"
          type="submit"
          btn240
        />
      </Form>
    </Modal>
  );
}

export default Index;
