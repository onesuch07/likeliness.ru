import { compose, bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import * as actionsTasks from 'models/tasks/actions';
import * as actionsHeader from 'models/header/actions';
import index from './index';

const mapStateToProps = state => ({
  ...state.tasks,
  ...state.header
});

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators({
    ...actionsTasks,
    ...actionsHeader
  }, dispatch)
});

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  reduxForm({
    enableReinitialize: false,
    form: 'createTask',
    onSubmit: (value, dispatch) => dispatch(actionsTasks.createTask(value))
  })
)(index);
