import { compose, bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { reduxForm, reset } from 'redux-form';
import * as actions from 'models/header/actions';
import index from './index';

const mapStateToProps = state => ({
  ...state.header
});

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators(actions, dispatch)
});

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  reduxForm({
    enableReinitialize: false,
    form: 'filterRegions',
    onChange: (value, dispatch) => dispatch(actions.filterRegions(value.location))
  })
)(index);