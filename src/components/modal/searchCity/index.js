import React from 'react';
import { Field } from 'redux-form';
import { map } from 'lodash/collection';
import { required, minLength2, uncensored, charactersLocked } from 'helpers/validators';
import { top18 } from './i18n';
import './styles.less';

import Form from 'components/form';
import FC from 'components/form/components';
import Modal from 'components/modal/Container';

function index({ handleSubmit, filterRegions, regions, activeCity }) {
  return (
    <Modal>
      <Form handleSubmit={handleSubmit} icon>
        <Form.H titleH="Выберите Ваш город" />

        <Field
          component={FC.InputDataList}
          validate={[required, minLength2, uncensored, charactersLocked]}
          name="location"
          icon="fa-search"
          type="text"
          placeholder={activeCity}
          value={activeCity}
          options={regions}
          objKey="city"
        />

        <ul className="list">
          {map(top18, (item, i) => (
            <li
              key={i}
              onClick={() => filterRegions(item)}
            >{item}</li>
          ))}
        </ul>
      </Form>
    </Modal>
  );
}

export default index;
