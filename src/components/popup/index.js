import React from 'react';
import Button from 'components/button';
import './styles.less';

function Popup({ message, eventNo, popupLocation }) {
  return (
    <div className="popup">
      <div className="message">{message}</div>
      <div className="controllers">
        <Button 
          label="Да, верно"
          event={popupLocation}
          runFunc
        />
        <Button
          label="Нет"
          event={eventNo}
          runFunc
        />
      </div>
    </div>
  );
}

export default Popup;
