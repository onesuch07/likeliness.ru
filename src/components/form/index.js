import React from 'react';
import { Link } from 'react-router-dom';
import classnames from 'classnames';
import Button from 'components/button';
import './style.less';

function H({ titleH, titleD }) {
  return (
    <div className="form-title">
      {titleH ? <h4>{titleH}</h4> : null}
      {titleD ? <p>{titleD}</p> : null}
    </div>
  );
}

function F({
  event,
  urlName,
  descriptionF,
  btnName,
  type,
  btn240,
  btnT,
  invalid,
  anyTouched,
  router,
  runFunc
}) {
  const mw240 = btn240 || null;
  return (
    <React.Fragment>
      <div className={classnames('get-code', !btnT && 'mt20')}>
        {router ? (
          <Link to={router}>
            <Button
              btn240={mw240}
              type={type}
              label={btnName}
              invalid={invalid}
              anyTouched={anyTouched}
            />
          </Link>
        ) : (
          <Button
            btn240={mw240}
            type={type}
            label={btnName}
            invalid={invalid}
            anyTouched={anyTouched}
            event={event}
            runFunc={runFunc}
          />
        )}
      </div>
      <div className="replace-form">
        {descriptionF ? <p>{descriptionF}</p> : null}
        {urlName ? <a onClick={() => event()}>{urlName}</a> : null}
      </div>
    </React.Fragment>
  );
}

function Form({ children, handleSubmit, row, icon }) {
  return (
    <form onSubmit={handleSubmit} className={classnames(
      'form',
      row && 'row',
      icon && 'icon'
    )}>
      {children}
    </form>
  );
}

Form.H = H;
Form.F = F;

export default Form;
