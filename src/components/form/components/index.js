import Input from './Input';
import TextArea from './TextArea';
import File from './File';
import Select from './Select';
import InputDate from './InputDate';
import InputDataList from './InputDataList';
import TextAreaInputFile from './TextAreaInputFile';

export default {
  Input,
  TextArea,
  File,
  Select,
  InputDate,
  InputDataList,
  TextAreaInputFile
};
