import React from 'react';
import classnames from 'classnames';
import { map } from 'lodash/collection';

function index(props) {
  const { touched, error, active } = props.meta;
  const tagAttributes = {
    ...props.input,
    name: props.input.name,
    id: props.input.name,
    type: props.type,
    placeholder: props.placeholder
  };
  const validation = touched && error ? 'invalid' : null;
  const focus = active ? ' active' : null;

  return (
    <label htmlFor={tagAttributes.id}>
      {props.label}
      {props.icon && <i className={`fa ${props.icon}`} aria-hidden="true" />}
      <input
        {...tagAttributes}
        list={`${tagAttributes.id}-list`}
        className={classnames('inp-list', focus, validation)}
        onChange={props.input.onChange}
      />
      <datalist id={`${tagAttributes.id}-list`}>
        {map(props.options, (e, i) => <option key={i} value={e[props.objKey]} />)}
      </datalist>
    </label>
  );
}

export default index;