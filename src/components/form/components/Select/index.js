import React from 'react';
import classnames from 'classnames';

function index(props) {
  const { touched, error, active } = props.meta;
  const validation = touched && error ? 'invalid' : null;
  const focus = active ? ' active' : null;
  const tagAttributes = {
    ...props.input,
    name: props.input.name,
    id: props.input.name
  };

  return (
    <label htmlFor={tagAttributes.id}>
      {props.label}
      <select
        {...tagAttributes}
        className={classnames(validation, focus)}
        onChange={props.input.onChange}
        onFocus={props.input.onFocus}
      >
        {props.items.map((e, i) => (
          <option key={i} value={e.name || e}>
            {e.name || e}
          </option>
        ))}
      </select>
    </label>
  );
}

export default index;
