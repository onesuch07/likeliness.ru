import React from 'react';
import classnames from 'classnames';
import './style.less';

function index(props) {
  const { touched, error, active } = props.meta;
  const tagAttributes = {
    ...props.input,
    name: props.input.name,
    id: props.input.name,
    type: props.type,
    placeholder: props.placeholder
  };
  const validation = touched && error ? 'invalid' : null;
  const focus = active ? ' active' : null;

  return (
    <label htmlFor={tagAttributes.id}>
      {props.label}
      {props.icon && <i className={`fa ${props.icon}`} aria-hidden="true" />}
      <input {...tagAttributes} className={classnames(validation, focus)} />
      {props.children}
    </label>
  )
}

export default index;
