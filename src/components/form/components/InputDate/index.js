import React from 'react';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import MomentLocaleUtils, {
  formatDate,
  parseDate
} from 'react-day-picker/moment';
import classnames from 'classnames';
import './style.less';

import 'moment/locale/ru';

function index(props) {
  const { touched, error, active } = props.meta;
  const tagAttributes = {
    ...props.input,
    name: props.input.name,
    id: props.input.name
  };
  const validation = touched && error ? 'invalid' : null;
  const focus = active ? ' active' : null;
  const format = 'DD.MM.YYYY';

  return (
    <label htmlFor={tagAttributes.id}>
      {props.label}
      <DayPickerInput
        formatDate={formatDate}
        parseDate={parseDate}
        format={format}
        placeholder={`${formatDate(new Date(), format)}`}
        locale="ru"
        localeUtils={MomentLocaleUtils}
        onDayChange={props.input.onChange}
        dayPickerProps={{
          locale: 'ru',
          localeUtils: MomentLocaleUtils,
          className: classnames(validation, focus),
        }}
      />
    </label>
  );
}

export default index;
