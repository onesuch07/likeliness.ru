import React from 'react';
import classnames from 'classnames';

function index(props) {
  const { touched, error, active } = props.meta;
  const tagAttributes = {
    name: props.input.name,
    id: props.input.name,
    type: props.type,
    disabled: props.disabled,
    accept: props.input.accept
  };
  const validation = touched && error ? 'invalid' : null;
  const focus = active ? ' active' : null;

  return props.type == 'file' ? (
    <div className="file">
      <label htmlFor={tagAttributes.name}>
        <input {...tagAttributes} className={classnames(focus, validation)} />
        <p>{props.label}</p>
      </label>
    </div>
  ) : null;
}

export default index;
