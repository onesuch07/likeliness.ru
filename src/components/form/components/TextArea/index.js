import React from 'react';
import classnames from 'classnames';
import './style.less';

function index(props) {
  const { touched, error, active } = props.meta;
  const tagAttributes = {
    ...props.input,
    name: props.input.name,
    id: props.input.name,
    placeholder: props.placeholder,
    disabled: props.disabled
  };
  const validation = touched && error ? 'invalid' : null;
  const focus = active ? ' active' : null;
  return (
    <label htmlFor={props.name}>
      {props.label}
      <textarea 
        {...tagAttributes}
        className={classnames(validation, focus)}
      >{props.input.value}</textarea>
    </label>
  );
}

export default index;
