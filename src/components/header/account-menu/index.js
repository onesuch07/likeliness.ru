import React from 'react';
import { Link } from 'react-router-dom';
import * as routers from 'helpers/routers';
import './style.less';

function index({ showMenu, logout, closePopup, modalChat }) {
  return showMenu ? (
    <ul className="account-menu">
      <li className="menu-close">
        <svg onClick={() => closePopup()}><use xlinkHref="/sprite.svg#close"/></svg>
      </li>
      <li onClick={() => closePopup()}>
        <Link to={routers.myTasks()}>
          <svg><use xlinkHref="/sprite.svg#docs"/></svg>
          <p>Мои задачи</p>
        </Link>
      </li>
      <li onClick={() => modalChat()}>
        <Link to="">
          <svg enableBackground="new 0 0 512 512" height="512" viewBox="0 0 512 512">
            <use xlinkHref="/sprite.svg#chats"/>
          </svg>
          <p>Чаты</p>
        </Link>
      </li>
      <li onClick={() => closePopup()}>
        <Link to={routers.defaultRouting()}>
          <svg><use xlinkHref="/sprite.svg#settings"/></svg>
          <p>Настройки аккаунта</p>
        </Link>
      </li>
      <li onClick={() => {
        logout();
        closePopup();
      }}>
        <Link to={routers.defaultRouting()}>
          <svg><use xlinkHref="/sprite.svg#power"/></svg>
          <p>Выход</p>
        </Link>
      </li>
    </ul>
  ) : null;
}

export default index;