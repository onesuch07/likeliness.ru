import React from 'react';
import { Link } from 'react-router-dom';
import classnames from 'classnames';
import * as routers from 'helpers/routers';
import './style.less';

import Popup from 'components/popup';
import SignIn from 'components/modal/signIn/Container';
import SignUp from 'components/modal/signUp/Container';
import CreateTask from 'components/modal/createTask/Container';
import YouPerformer from 'components/modal/youPerformer/Container';
import SearchCity from 'components/modal/searchCity/Container';
import Chat from 'components/modal/chat/Container';
import AccountMenu from 'components/header/account-menu';

import Logo from 'assets/svg-images/logo';

class Header extends React.Component {
  constructor(props) {
    super(props);
    
    this.refHeader = React.createRef();
    this.state = {
      fixedHeader: false
    };
  }
  
  componentWillMount() {
    document.addEventListener('scroll', () => {
      if (
        this.refHeader && this.refHeader['current'] &&
        window.scrollY > this.refHeader.current.offsetHeight + 20
      ) {
        this.setState({ fixedHeader: true });
      } else {
        this.setState({ fixedHeader: false });
      }
    })
  }

  render() {
    const {
      modals,
      popups,
      popupAccountMenu,
      popupLocation,
      account,
      activeCity,
      logout,
      modalSignIn,
      modalSignUp,
      modalCreateTask,
      modalSearchCity,
      modalChat
    } = this.props;
    const { fixedHeader } = this.state;
    
    return (
      <div className={classnames('header-container', fixedHeader && 'container-fixed')}>
        <div className="max-size-windows">
          <div
            ref={this.refHeader}
            className="app-header"
          >
            <div className="left-position">
              <div className="logo">
                <Link to={routers.defaultRouting()}>
                  <Logo />
                </Link>
              </div>
              <div className="wrapper-popup">
                <div
                  className="location"
                  onClick={() => modalSearchCity()}
                >
                  <svg><use xlinkHref="/sprite.svg#location"/></svg>
                  <p>{activeCity}</p>
                </div>
                {(activeCity !== 'Регион' && popups['location']) && (
                  <Popup
                    popupLocation={popupLocation}
                    message={`Ваш город ${activeCity}?`}
                    eventNo={modalSearchCity}
                  />
                )}
              </div>
              <div className="my-works">
                <Link to={routers.tasks()}>Все задания</Link>
                {account['name'] && (
                  <Link to={routers.myTasks()}>Мои задачи</Link>
                )}
              </div>
            </div>
            <div className="right-position">
              {!account['name'] ? (
                <div className="login">
                  <a onClick={() => modalSignIn()}>Войти</a>
                  <span>/</span>
                  <a onClick={() => modalSignUp()}>Зарегистрироваться</a>
                </div>
              ) : (
                <React.Fragment>
                  <div className="user">
                    <div className="popup-menu" onClick={() => popupAccountMenu()}>
                      <p>{account.name}</p>
                      <span className={popups['accountMenu'] ? 'rotate' : null}>
                      <svg><use xlinkHref="/sprite.svg#pointer"/></svg>
                    </span>
                    </div>
                    <AccountMenu
                      showMenu={popups['accountMenu']}
                      logout={logout}
                      closePopup={popupAccountMenu}
                      modalChat={modalChat}
                    />
                  </div>
                  <div className="create-task">
                    <a onClick={() => modalCreateTask()}>Создать задание</a>
                  </div>
                </React.Fragment>
              )}
            </div>

            {modals.signIn ? <SignIn /> : null}
            {modals.signUp ? <SignUp /> : null}
            {modals.createTask ? <CreateTask /> : null}
            {modals.youPerformer ? <YouPerformer /> : null}
            {modals.searchCity ? <SearchCity /> : null}
            {modals.chat ? <Chat /> : null}
          </div>
        </div>
      </div>
    );
  }
}

export default Header;
