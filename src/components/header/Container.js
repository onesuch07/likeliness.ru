import { compose, bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionsHeader from 'models/header/actions';
import * as actionsAccount from 'models/account/actions';
import index from './index';

const mapStateToProps = state => ({
  ...state.header,
  account: state.account
});

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators({
    ...actionsHeader,
    ...actionsAccount
  }, dispatch)
});

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(index);
