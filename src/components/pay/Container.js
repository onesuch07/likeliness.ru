import { compose } from 'redux';
import { connect } from 'react-redux';
import index from './index';

const mapStateToProps = state => ({
  task: state.tasks.task
});

export default compose(connect(mapStateToProps))(index);
