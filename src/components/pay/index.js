import React from 'react';
import { Redirect } from 'react-router-dom';
import * as routers from 'helpers/routers';
import './styles.less';

const iframeAttributes = {
  width: '100%',
  height: 227,
  frameBorder: 0,
  allowtransparency: true,
  scrolling: 'no',
};

class Pay extends React.PureComponent {
  componentDidMount() {
    const { task } = this.props;
    if (task) {
      const iframe = document.createElement('iframe');
      const payContainer = document.querySelector('.pay-container');
      
      for (const key in iframeAttributes) iframe.setAttribute(key, iframeAttributes[key]);
      iframe.setAttribute('src', 'https://money.yandex.ru/quickpay/shop-widget?writer=buyer&' +
        'targets=&targets-hint=%D0%A4%D0%BE%D1%80%D0%BC%D0%B0%D1%82%3A%200000%200000%200000%200000&button-text=12&payment-type-choice=on&' +
        'hint=&successURL=http%3A%2F%2Flikeliness.ru&quickpay=shop&account=4100115816540308&default-sum=' + task.price);
      payContainer.appendChild(iframe);
    }
  }

  render() {
    const { task } = this.props;
    return task ? (
      <div className="pay">
        <div className="pay-container">
          <div className="pay-title">
            <h4>Оформление заказа</h4>
          </div>
        </div>
      </div>
    ) : <Redirect to={routers.defaultRouting()} />
  }
}

export default Pay;