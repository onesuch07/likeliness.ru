import React from 'react';
import { Link } from 'react-router-dom';
import { map } from 'lodash/collection';
import './styles.less';

function index({ tabs, tabEvent, activeTab }) {
  return (
    <ul className="tabs">
      {tabs && tabEvent ? map(tabs, (el, i) => (
        <li key={i}>
          <Link
            key={i}
            className={(activeTab && i === activeTab.id) ? 'active-tab' : null}
            onClick={()=> tabEvent(el, i)}
            to={el.path}
          >{el.title}</Link>
        </li>
      )) : null}
    </ul>
  );
}

export default index;
