import React from 'react';
import { map } from 'lodash/collection';
import classnames from 'classnames';
import moment from 'moment';
import './styles.less';

import TaskStatus from 'components/taskStatus';

function index({ tasks, mwnone, userTaskUpdate, userId }) {
  const w = mwnone && 'mwnone';
  return tasks ? map(tasks, (item, i) => (
        <div className={classnames('task', w)} key={i}>
          <div className="task-title">
            <ul className="task-left">
              <li className="task-proffession helvetica-neue">{item.title}</li>
              <li className="task-data">
                {moment(item.taskCreate).format('DD.MM.YYYY')}&nbsp;
                (актуален до {moment(item.taskActive).format('DD.MM.YYYY')})
              </li>
            </ul>
            <ul className="task-right">
              <li>
                <TaskStatus status={item.status} />
              </li>
            </ul>
          </div>
          <div className="task-body">
            <p>{item.description}</p>
          </div>
          <div className="icons">
            {item.gallery ? item.gallery.map((el, i) => 
              <img src={el.url} alt="icon" key={i} />
            ) : null}
          </div>
          <div className="task-footer">
            <ul className="task-footer-left">
              <li>
                Город: <p className="lucida-grande">{item.location}</p>
              </li>
            </ul>
            { item.status !== 3 ? (
              <ul className="task-footer-right">
                <li>
                  {item.price} <p className="lucida-grande">р.</p>
                </li>
                <li>
                  {item.status === 1 && (
                    <button
                      onClick={() => userTaskUpdate(item['_id'], 2, item.userId, item.userTaskProgress)}
                    >Выполнить</button>
                  )}
                  {(item.status === 2 && userId === item.userId) && (
                    <button
                      onClick={() => userTaskUpdate(item['_id'], 3, item.userId, item.userTaskProgress, true)}
                    >Проверить</button>
                  )}
                </li>
              </ul>
            ) : null }
          </div>
        </div>
      ))
    : null;
}

export default index;
