import React from 'react';
import Customer from 'assets/svg-images/customer';
import Freelance from 'assets/svg-images/freelance';
import Handshake from 'assets/svg-images/customer-handshake';
import Executor from 'assets/svg-images/executor';
import './style.less';

function Advantages() {
  return (
    <ul className="app-advantages">
      <li>
        <div className="list-number">
          <Customer />
          <h6>Заказчик</h6>
          <p>Размещает свой заказ за пару минут</p>
        </div>
      </li>
      <li>
        <div className="list-number">
          <Freelance />
          <h6>Фрилансеры</h6>
          <p>Предлагают услуги, цены и сроки</p>
        </div>
      </li>
      <li>
        <div className="list-number">
          <Handshake />
          <h6>Заказчик</h6>
          <p>Выбирает подходящего исполнителя</p>
        </div>
      </li>
      <li>
        <div className="list-number">
          <Executor />
          <h6>Исполнитель</h6>
          <p>Выполняет работу и получает оплату</p>
        </div>
      </li>
    </ul>
  );
}

export default Advantages;
