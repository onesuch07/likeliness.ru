import React from 'react';

function index({ status }) {
  switch (status) {
    case 1:
      return (
        <p className="task-status-circle">
          <span className="task-circle task-active" />
          Активен
        </p>
      );
    case 2:
      return (
        <p className="task-status-circle">
          <span className="task-circle task-expectation" />
          Ожидание
        </p>
      );
    case 3:
      return (
        <p className="task-status-circle">
          <span className="task-circle task-complited" />
          Завершено
        </p>
      );
  }
}

export default index;
