import React from 'react';
import classnames from 'classnames';
import './style.less';

function index(props) {
  const {
    className,
    disabled,
    type,
    label,
    id,
    btn240,
    invalid, 
    anyTouched,
    event,
    runFunc
  } = props;
  const inputAttributes = {
    disabled,
    type,
    id
  };
  const noValid = (invalid && anyTouched) && 'no-valid';
  const minW240 = btn240 && 'min-w-240';

  return (
    <button
      {...inputAttributes}
      className={classnames('button-model', className, noValid, minW240)}
      onClick={() => runFunc ? event() : event}
    >
      {label}
    </button>
  );
}

export default index;