import React from 'react';
import './style.less';

function index() {
  return (
    <div className="app-footer">
      <p>© 2019 - LikeLiness. Все права защищены.</p>
    </div>
  );
}

export default index;
