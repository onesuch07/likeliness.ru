import React from 'react';

const Freelance = (props) => (
  <svg width={67} height={58} {...props}>
    <g fill="none" fillRule="evenodd">
      <ellipse fill="#D7D7D7" cx={12.06} cy={12.938} rx={5.807} ry={5.8} />
      <path
        d="M40.83 19.725h7.373M40.83 30.432h7.373"
        stroke="#D7D7D7"
        strokeWidth={0.892}
      />
      <path
        fill="#EDEDED"
        d="M31.267 12.492h9.827V31.23h-9.827zM47.347 7.138h9.826v24.093h-9.826z"
      />
      <path
        d="M4.815 22.114h29.413c-1.298 3.323-2.393 4.984-3.285 4.984H19.239V57.01h-6.552V41.275 57.01H4.9V42.447C1.869 41.666.353 40.704.353 39.562V26.576a4.462 4.462 0 0 1 4.462-4.462z"
        fill="#4A90E2"
      />
      <path
        d="M4.467 28.554v13.655M11.836 43.723V57.38"
        stroke="#FFF"
        strokeWidth={0.892}
        fill="#4A90E2"
      />
      <path
        d="M24.119 18.24l9.113 18.345h31.981c.495 0 .895-.4.895-.893V1.785c0-.493-.4-.893-.895-.893h-40.2c-.494 0-.894.4-.894.893V18.24z"
        stroke="#D7D7D7"
        strokeWidth={1.785}
      />
      <path
        fill="#D8D8D8"
        d="M24.12 35.692h2.68v2.677h-2.68zM27.693 35.692h2.68v2.677h-2.68z"
      />
    </g>
  </svg>
);

export default Freelance;
