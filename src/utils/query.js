import * as React from 'react';

export default (promise, propsFn, disableMountFetch) => (
  WrappedComponent,
  mapFn
) =>
  class Promised extends React.Component {
    constructor(props) {
      super(props);
      this.state = { loading: true, error: null, data: null };
    }

    componentDidMount() {
      if (disableMountFetch == true) {
        this.setState({
          loading: false,
          data: [],
        });
      } else this.refetch();
    }

    refetch = (variables = {}) => {
      let payload = { ...variables };

      if (propsFn) {
        payload = {
          ...payload,
          ...propsFn(this.props),
        };
      }

      promise(payload).then(
        (data) => {
          let result = data;
          if (mapFn) {
            result = mapFn(data);
          }

          return this.setState({ loading: false, data: result });
        },
        (error) => this.setState({ loading: false, error })
      );
    };

    render() {
      const withDataProps = { ...this.props, ...this.state };
      return <WrappedComponent {...withDataProps} refetch={this.refetch} />;
    }
  };
