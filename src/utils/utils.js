import get from 'lodash/get';

const Utils = {
  formatDate(date) {
    const year = date.getFullYear();
    const month = `0${date.getMonth() + 1}`.slice(-2);
    const day = `0${date.getDate()}`.slice(-2);
    return `${year}-${month}-${day}`;
  },

  updateObject(oldObject, updatedProperties) {
    return {
      ...oldObject,
      ...updatedProperties,
    };
  },

  isChildLink(currentLocation, linkToCheck) {
    return currentLocation.indexOf(linkToCheck) >= 0;
  },

  prepearHeaderForUploadFeatch(json) {
    delete json['Accept'];
    delete json['Content-type'];
    return json;
  },

  getFloat(obj, field, defVal) {
    return this.formatCostSpaces(get(obj, field, defVal), 2, defVal);
  },

  getInt(obj, field, defVal) {
    return this.formatCostSpaces(get(obj, field, defVal), 0, defVal);
  },

  formatCostSpaces(number, digits = 2, defVal = number) {
    const addSpaces = (nStr) => {
      const remainder = nStr.length % 3;
      return (
        nStr.substr(0, remainder) +
        nStr.substr(remainder).replace(/(\d{3})/g, ' $1')
      ).trim();
    };

    const s = parseFloat(Math.round(number * 100) / 100).toFixed(digits);
    if (isNaN(s)) return defVal;
    return addSpaces(s.toString().replace('.', ','));
  },

  htmlId() {
    return `id-${this.guid()}`;
  },

  guid() {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }

    return s4() + s4() + s4() + s4() + s4() + s4() + s4() + s4();
  },

  getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  },

  parseFloat(string, defaultValue) {
    if (!parseFloat(string)) return defaultValue;

    return parseFloat(string);
  },

  formatCost(number) {
    const s = parseFloat(Math.round(number * 100) / 100).toFixed(2);
    if (isNaN(s))
      return parseFloat(0)
        .toFixed(2)
        .toString()
        .replace('.', ',');
    return s.toString().replace('.', ',');
  },

  formatWeight(weight) {
    return parseFloat(Math.round(weight * 1000) / 1000).toFixed(3);
  },

  firstKey(obj) {
    for (const key in obj) {
      if (obj.hasOwnProperty(key)) return key;
    }
  },

  emailValidate(email) {
    // eslint-disable-next-line
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  },

  passwordValidate(password) {
    if (!password) return false;
    return password.length >= 4;
  },

  trace(message, arg1, arg2, arg3) {
    if (this.TRACE) {
      // eslint-disable-next-line
      console.trace(
        message,
        arg1 ? arg1 : '',
        arg2 ? arg2 : '',
        arg3 ? arg3 : ''
      );
    }
  },

  DISPLAY_DATE_FORMAT: 'DD.MM.YYYY',
  DISPLAY_DATETIME_FORMAT: 'HH:mm DD.MM.YYYY',

  DATE_FORMAT: 'DD.MM.YYYY',
  DATE_PATTERN: '11.11.1111',
  TIME_RANGE_MASK: '11:11 - 11:11',
  TIME_FORMAT: 'HH:mm',
  DATETIME_FORMAT: 'HH:mm DD.MM.YYYY',

  DATEPICKER_LOCALE: {
    fromLabel: 'От:',
    toLabel: 'До:',
    applyLabel: 'Применить',
    cancelLabel: 'Отмена',
    customRangeLabel: 'Другой',
  },
};

export default Utils;
