const main = document.createElement('div');
main.className = 'toastr';
document.querySelector('body').append(main);

function createTag(message, st) {
  let div = document.createElement('div');
  let i = document.createElement('i');
  const status = st == 'success' ? 'fa-check' : st == 'warning' ? 'fa-exclamation' : 'fa-times';
  i.className = `fa ${status}`;
  i.setAttribute('aria-hidden', 'true');
  div.className = st;
  div.innerText = message;
  div.prepend(i);
  main.append(div);
  setTimeout(() => div.remove(), 5000)
}

window.success = (message) => createTag(message, 'success');
window.warning = (message) => createTag(message, 'warning');
window.error = (message) => createTag(message, 'error');
