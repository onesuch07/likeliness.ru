import pathToRegexp from 'path-to-regexp';

function pathMatchRegexp(regexp, pathname) {
  return pathToRegexp(regexp).exec(pathname);
}

function listenHistory(props, path, cb) {
  let initialLoad = true;
  const { pathname } = props.history.location;

  props.history.listen((location) => {
    initialLoad = false;
    const match = pathMatchRegexp(path, location.pathname);
    if (match) {
      cb(match[1], match);
    }
  });

  if (initialLoad) {
    const match = pathMatchRegexp(path, pathname);
    if (match) {
      cb(match[1], match);
    }
  }
}

export default listenHistory;
